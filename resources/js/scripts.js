function isPresent($el, callback) {
    if ($el.length) {
        callback($el)
    }
}

(function ($, undefined) {
    $(document).ready(function () {

        if ($(window).width() > '1024'){
            initFullPage();
        } else {

        }

        //video mute
        $("video").prop("muted", true);
        $(".btn-mute").click(function() {
        $(".btn-mute").toggleClass("enable-mute");
            if ($("video").prop("muted")) {
            $("video").prop("muted", false);


        } else {
                $("video").prop("muted", true);
            }
        });

        //menu add class
        var trigger = $('.burger-box');
        var menuBox = $('.menu-box');
        var body = $('body');

        trigger.click(function () {
            menuBox.toggleClass('open');
            trigger.toggleClass('open');
            body.toggleClass('no-scroll');
        });

        menuBox.click(function (){
            menuBox.toggleClass('open');
            trigger.toggleClass('open');

        });

        //fullpage initial
        function initFullPage() {
            isPresent($('#fullPage'), function ($el) {

                var fullpageSectionsIds = [];

                function createFullpageNavigation() {

                    const fullpageNavmenu = document.createElement('ul');
                    fullpageNavmenu.setAttribute('class', 'fullpage-navmenu');
                    fullpageNavmenu.setAttribute('id', 'fullpageNavmenu');

                    $('#fullPage .section').each(function () {

                        const sectionId = $(this).attr('id') + '_fullpage';
                        const title = $(this).data('name');

                        fullpageSectionsIds.push(sectionId);

                        const menuItem = document.createElement('li');
                        const menuLink = document.createElement('a');
                        const menuLine = document.createElement('span');

                        menuLink.setAttribute('class', 'fullpage-navmenu__link');
                        menuLink.setAttribute('href', '#' + sectionId);
                        menuLink.setAttribute('data-menuanchor', sectionId);
                        menuLink.textContent = title;
                        menuItem.appendChild(menuLink);
                        menuLink.appendChild(menuLine);
                        fullpageNavmenu.appendChild(menuItem);
                    });

                    document.querySelector('.page-wrapper').appendChild(fullpageNavmenu);
                }

                function initializationFullPage() {
                    $('.fullpage-navmenu').show();

                    $el.fullpage({
                        touchSensitivity: 5,
                        navigation: false,
                        anchors: fullpageSectionsIds,
                        menu: '#fullpageNavmenu',
                        verticalCentered: true,
                        scrollingSpeed: 1000,
                    });
                }

                createFullpageNavigation();

                initializationFullPage();

                $(window).resize(function () {
                    $.fn.fullpage.reBuild();
                });

            });
        };

        //slider initial
        $('.slider-speaker').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            fade: true,
            arrows: false,
            asNavFor: '.slider-nav',
            responsive: [
                {
                    breakpoint: 1024,
                    settings: {
                        arrows: true,
                        fade: false
                    }
                }
            ]
        });
        $('.slider-nav').slick({
            slidesToShow: 3,
            slidesToScroll: 1,
            asNavFor: '.slider-speaker',
            dots: true,
            centerMode: true,
            focusOnSelect: true,
            arrows: true,
            responsive: [
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 3,
                        infinite: true,
                        arrows: false
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 2
                    }
                }
            ]
        });
    });
})(jQuery);




